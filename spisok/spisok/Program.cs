﻿using System;

namespace spisok
{
    class Node
    {
        public int value;
        public Node next;
        public Node(int x)
        {
            this.value = x;
        }
    }

    class LList
    {
        public Node head;
        public int Count;
        public LList()
        {

        }

        public void AddLast(int x)
        {
            var toAdd = new Node(x);
            if (head == null)
            {
                head = toAdd;
            }
            else
            {
                Node current = head;
                while(current.next != null)
                {
                    current = current.next;

                }
                current.next = toAdd;
            }
            ++Count;
        }

        public void AddFirst(int x)
        {
            var toAdd = new Node(x);
            toAdd.next = head;
            head = toAdd;
            ++Count;
        }


        public void insert(int index, int x)
        {
            

            if (head == null)
            {
                var toAdd = new Node(x);
                head = toAdd;
                ++Count;
            }
            else
            {
                if(index == 0)
                {
                    AddFirst(x);
                }
                else
                {
                    var toAdd = new Node(x);
                    Node current = head;
                    Node previous = head;
                    for(int i = 0; i < index && current != null; i++)
                    {
                        previous = current;
                        current = current.next;
                    }
                    previous.next = toAdd;
                    toAdd.next = current;
                    ++Count;
                }
            }
            
            
        }


        public int this[int index]
        {
            get
            {
                Node current = head;
                for(int i = 0; i < index && current != null; i++)
                {
                    current = current.next;
                }
                return current.value;
            }
            set
            {
                Node current = head;
                for (int i = 0; i < index && current != null; i++)
                {
                    current = current.next;
                }
                current.value = value;

            }
        }


        public void Clear()
        {
            head = null;
            Count = 0;
        }


        public void Remove(int index)
        {
            if(head != null)
            {
                if(index == 0)
                {
                    head = head.next;
                    --Count;
                }
                else if(index < Count - 1)
                {
                    Node current = head;
                   
                    for (int i = 0; i < index - 1 && current.next != null; i++)
                    {
                        current = current.next;
                    }
                    current.next = current.next.next;
                    --Count;
                }
                else
                {
                    Node current = head;

                    for (int i = 0; i < index - 1 && current.next != null; i++)
                    {
                        current = current.next;
                    }
                    
                    current.next = null;
                    --Count;
                }
            }
        }

        

    }


    class MyQueue
    {
        public Node front;
        public Node rear;

        public void enqueue(int x)   //добавление
        {
            var toAdd = new Node(x);
            if(rear == null)
            {
                front = rear = toAdd;
            }
            else
            {
                rear.next = toAdd;
                rear = toAdd;
            }
        }

        public void denqueue()   //достать из очереди
        {
            if (front == null)
            {
                return;
            }
            var temp = front;
            front = this.front.next;

            if(front == null)
            {
                rear = null;
            }
        }

    }





    class MainClass
    {
        static void Main()
        {

            //FIFO


            var lst = new MyQueue();

            lst.enqueue(1);
            lst.enqueue(2);
            lst.enqueue(3);
            lst.enqueue(4);

            Console.WriteLine(lst.front.value);
            lst.denqueue();
            lst.denqueue();
            lst.enqueue(5);
            lst.denqueue();
            Console.WriteLine(lst.front.value);
            

            //var lst = new LList();
            //lst.AddLast(1);
            //lst.AddLast(2);
            //lst.AddLast(3);
            //lst.AddFirst(0);


            //lst.insert(2, 4);

            ///*Console.WriteLine(lst.head.value);
            //Console.WriteLine(lst.head.next.value);
            //Console.WriteLine(lst.head.next.next.value);*/

            //for (int i =0; i != lst.Count; i++)
            //{
            //    Console.Write(lst[i] + "->");
            //}
            //Console.WriteLine("null");

            //lst.Remove(3);
            //for (int i = 0; i != lst.Count; i++)
            //{
            //    Console.Write(lst[i] + "->");
            //}
            //Console.WriteLine("null");

        }
    }
}
